>DML:数据操纵语言DML(Data Manipulation Language),
    关键词：insert update delete

>DQL:数据查询语言DQL(Data Query Language),
    关键字：select

>DDL:数据定义语言DDL(Data Definition Language),
    关键词： create alter drop truncate

>DCL:数据控制语言DCL(Data Control Language),
    grant：授权，
    rollback：回滚。
    commit：提交